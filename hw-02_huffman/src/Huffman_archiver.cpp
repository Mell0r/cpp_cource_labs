#include "Huffman_archiver.h"
#include "Huffman_code.h"
#include "bit_IO.h"

using std::vector;
using std::string;
using std::pair;

namespace huffman_archiver {
    using huffman_code::Huffman_code;
    using huffman_archiver::Coding_log;
    using huffman_archiver::Huffman_archiver;

    Coding_log Huffman_archiver::encode(const string &in_file, const string &out_file) {
        bit_IO::Bit_reader reader(in_file);
        bit_IO::Bit_writer writer(out_file);
        std::map<char, int> freq;
        int input_size = 0;
        try {
            while (true) {
                freq[reader.read_byte()]++;
                input_size++;
            }
        } catch (const bit_IO::IO_Exception &e) {}

        if (input_size == 0)
            return {0, 0, 0};

        Huffman_code huffman_code(freq);

        auto n = (short) huffman_code.chars.size();

        writer.write_short(n);
        for (auto &ch: huffman_code.chars)
            writer.write_byte(ch);

        for (int i = n; i < huffman_code.tree.size(); i++) {
            writer.write_short(huffman_code.tree[i].l);
            writer.write_short(huffman_code.tree[i].r);
        }

        int text_len = 0;
        for (auto &ch: huffman_code.chars)
            text_len += (int) huffman_code.code[ch].size() * freq[ch];
        char offset = (char) ((bit_IO::BYTE_SIZE - text_len % bit_IO::BYTE_SIZE) % bit_IO::BYTE_SIZE);
        writer.write_byte(offset);
        writer.write_bit_string(std::string(offset, '0'));

        reader.to_begin();
        try {
            while (true) {
                writer.write_bit_string(huffman_code.code[reader.read_byte()]);
            }
        }
        catch (const bit_IO::IO_Exception &e) {}

        return {
                input_size,
                text_len / bit_IO::BYTE_SIZE + (text_len % bit_IO::BYTE_SIZE > 0),
                huffman_code.calc_additional_info_size()
        };
    }

    Coding_log Huffman_archiver::decode(const string &in_file, const string &out_file) {
        bit_IO::Bit_reader reader(in_file);
        bit_IO::Bit_writer writer(out_file);
        short n;
        try {
            n = reader.read_short();
        }
        catch (const bit_IO::IO_Exception &e) {
            return {0, 0, 0};
        }

        vector<char> chars(n);
        for (int i = 0; i < n; i++)
            chars[i] = reader.read_byte();
        vector<pair<int, int>> children(n - 1);
        for (int i = 0; i < n - 1; i++) {
            children[i].first = reader.read_short();
            children[i].second = reader.read_short();
        }

        Huffman_code huffman_code(chars, children);
        unsigned char offset = reader.read_byte();
        for (unsigned char i = 0; i < offset; i++)
            reader.read_bit();

        int pos = huffman_code.tree.size() - 1;
        int output_size = 0;
        int input_bit_size = 0;
        try {
            while (true) {
                bool bit = reader.read_bit();
                input_bit_size++;
                if (!bit)
                    pos = huffman_code.tree[pos].l;
                else
                    pos = huffman_code.tree[pos].r;
                if (pos < n) {
                    writer.write_byte(chars[pos]);
                    output_size++;
                    pos = huffman_code.tree.size() - 1;
                }
            }
        }
        catch (const bit_IO::IO_Exception &e) {}

        return {
                (input_bit_size + offset) / bit_IO::BYTE_SIZE,
                output_size,
                huffman_code.calc_additional_info_size()
        };
    }
}