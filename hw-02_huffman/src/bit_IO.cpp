#include "bit_IO.h"
#include <iostream>

using std::vector;

bit_IO::Bit_reader::Bit_reader(const std::string &filename) {
    file.open(filename);
    if (!file.is_open())
        throw IO_Exception("Couldn't open the given file.");
    cur_bit_rem = 0;
    buffer = 0;
}

void bit_IO::Bit_reader::update_buffer() {
    if (cur_bit_rem != 0)
        return;
    if (!file.read(&buffer, 1).good())
        throw IO_Exception("Couldn't read next byte.");
    cur_bit_rem = BYTE_SIZE;
}

char bit_IO::Bit_reader::read_byte() {
    char res = 0;
    for (int i = 0; i < BYTE_SIZE; i++)
        res = (char) (res << 1) + read_bit();
    return res;
}

bool bit_IO::Bit_reader::read_bit() {
    update_buffer();
    return (buffer >> (--cur_bit_rem)) & 1;
}

bit_IO::Bit_reader::~Bit_reader() {
    file.close();
}

short bit_IO::Bit_reader::read_short() {
    short res = (unsigned char)read_byte();
    return (short)(res << BYTE_SIZE) + (unsigned char)read_byte();
}

void bit_IO::Bit_reader::to_begin() {
    file.clear();
    file.seekg(0);
}

bit_IO::Bit_writer::Bit_writer(const std::string &filename) {
    file.open(filename);
    if (!file.is_open())
        throw IO_Exception("Couldn't open the given file.");
    cur_bit_writen = 0;
    buffer = 0;
}

void bit_IO::Bit_writer::push_buffer() {
    if (cur_bit_writen < BYTE_SIZE)
        return;
    if (!file.write(&buffer, 1).good())
        throw IO_Exception("Couldn't write next byte.");
    cur_bit_writen = 0;
}

void bit_IO::Bit_writer::write_byte(char byte) {
    for (int i = BYTE_SIZE - 1; i >= 0; i--)
        write_bit((byte >> i) & 1);
}

void bit_IO::Bit_writer::write_bit(bool bit) {
    buffer = (char)((buffer << 1) + bit);
    cur_bit_writen++;
    push_buffer();
}

void bit_IO::Bit_writer::write_short(short n) {
    write_byte(n >> BYTE_SIZE);
    write_byte(n % 256);
}

void bit_IO::Bit_writer::write_bit_string(const std::string& s) {
    for (char c : s)
        write_bit(c - '0');
}

bit_IO::Bit_writer::~Bit_writer() {
    file.close();
}
