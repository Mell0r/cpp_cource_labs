#include "Huffman_code.h"

#include <algorithm>
#include <queue>

using std::map;
using std::pair;
using std::string;
using std::vector;

namespace huffman_code {


    void Huffman_code::calc_codes(int pos, const string &cur_code) {
        if (pos == utils::NONE)
            return;

        if (tree[pos].l == utils::NONE && tree[pos].r == utils::NONE) {
            code[chars[pos]] = cur_code;
            return;
        }

        calc_codes(tree[pos].l, cur_code + '0');
        calc_codes(tree[pos].r, cur_code + '1');
    }

    Huffman_code::Huffman_code(const map<char, int> &freq) noexcept {
        for (auto e: freq)
            chars.push_back(e.first);

        if (chars.size() == 1) {
            code[chars[0]] = "0";
            return;
        }

        auto comp = [&, this](int i1, int i2) {
            return (tree[i1].char_quantity > tree[i2].char_quantity);
        };

        std::priority_queue<int, vector<int>, decltype(comp)> pq(comp);

        for (auto &e: freq) {
            tree.emplace_back(e.second);
            pq.push((int) tree.size() - 1);
        }

        while (pq.size() > 1) {
            int l = pq.top();
            pq.pop();
            int r = pq.top();
            pq.pop();
            tree.emplace_back(tree[l].char_quantity + tree[r].char_quantity, l, r);
            pq.push((int) tree.size() - 1);
        }

        calc_codes((int) tree.size() - 1);
    }

    Huffman_code::Huffman_code(
            const vector<char> &chars,
            const vector<std::pair<int, int>> &children
    ) noexcept: chars(chars) {
        if (chars.size() == 1) {
            tree.resize(3);
            tree[2].l = 0;
            return;
        }

        int n = chars.size();
        tree.resize(2 * n - 1);
        for (int i = n; i < 2 * n - 1; i++)
            tree[i].l = children[i - n].first, tree[i].r = children[i - n].second;

        calc_codes((int) tree.size() - 1);
    }

    int Huffman_code::calc_additional_info_size() const noexcept {
        return chars.size() * 5 - 1;
    }
}