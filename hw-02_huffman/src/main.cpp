#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <Huffman_archiver.h>

using std::string;
using std::cout;
using std::endl;
using std::cerr;
using std::exception;

void check_active_flag(bool in_flag, bool out_flag) {
    if (in_flag) {
        cerr << "Incorrect arguments: empty input file name.";
        exit(0);
    }
    else if (out_flag) {
        cerr << "Incorrect arguments: empty output file name.";
        exit(0);
    }

}

void check_chosen_working_mod(const std::string& working_mode) {
    if (!working_mode.empty()) {
        cerr << "Incorrect arguments: double working mod chose.";
        exit(0);
    }
}

int main(int argc, char *argv[]) {
    huffman_archiver::Huffman_archiver archiver;
    string working_mod;
    string in_file = "", out_file = "";
    bool in_flag = false, out_flag = false;
    for (int i = 1; i < argc; i++) {
        string arg = (string)argv[i];
        if (arg.empty())
            continue;

        if (arg == "-f" || arg == "--file") {
            check_active_flag(in_flag, out_flag);
            in_flag = true;
        }
        else if (arg == "-o" || arg == "--output") {
            check_active_flag(in_flag, out_flag);
            out_flag = true;
        }
        else if (arg == "-c" || arg == "-u") {
            check_active_flag(in_flag, out_flag);
            check_chosen_working_mod(working_mod);
            working_mod = arg;
        }
        else if (in_flag) {
            if (!in_file.empty()) {
                cerr << "Incorrect arguments: double '-f' argument.";
                return 0;
            }
            in_file = arg;
            in_flag = false;
        }
        else if (out_flag) {
            if (!out_file.empty()) {
                cerr << "Incorrect arguments: double '-o' argument.";
            }
            out_file = arg;
            out_flag = false;
        }
        else {
            cerr << "Incorrect arguments: string without flag before it.";
            exit(0);
        }
    }
    try {
        huffman_archiver::Coding_log coding_log{};
        bool mark = false;
        if (working_mod == "-c") {
            coding_log = archiver.encode(in_file, out_file);
            mark = true;
        }
        else if (working_mod == "-u") {
            coding_log = archiver.decode(in_file, out_file);
            mark = true;
        }
        if (mark == 0) {
            cerr << "Incorrect arguments: working mod is incorrect.";
            return 0;
        }

        cout << coding_log.in_size << endl;
        cout << coding_log.out_size << endl;
        cout << coding_log.additional_size << endl;
    } catch(const exception& e) {
        cerr << e.what();
    }
}