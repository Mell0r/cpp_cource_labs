#include "doctest.h"
#include <string>
#include <fstream>

#include "Huffman_archiver.h"

using std::string;
using std::ifstream;

const string test_encoded_file = "../test/test_temp";
const string test_decoded_file = "../test/test_decoded_temp";
const string test_cases_path = "../samples/";
const auto archiver = huffman_archiver::Huffman_archiver::get_instance();

inline void byte_file_compare(const std::string& file_path1, const std::string& file_path2) {
    ifstream file1(file_path1);
    ifstream file2(file_path2);
    char c1;
    while (file1.read(&c1, 1).good()) {
        char c2;
        file2.read(&c2, 1);
        CHECK_EQ(c1, c2);
    }
    file1.close();
    file2.close();
}

TEST_CASE("archiver") {
    std::string test_case_path = test_cases_path;
    SUBCASE("empty") { test_case_path += "empty"; }
    SUBCASE("a") { test_case_path += "a"; }
    SUBCASE("aaaa") { test_case_path += "aaaa"; }
    SUBCASE("abab") { test_case_path += "abab"; }
    SUBCASE("even") { test_case_path += "even"; }
    SUBCASE("odd") { test_case_path += "odd"; }
    SUBCASE("12345") { test_case_path += "12345"; }
    SUBCASE("small_random") { test_case_path += "small_random"; }
    SUBCASE("5M_random") { test_case_path += "5M_random"; }

    CAPTURE(test_case_path);

    archiver.encode(test_case_path, test_encoded_file);
    archiver.decode(test_encoded_file, test_decoded_file);
    byte_file_compare(test_case_path, test_decoded_file);
}