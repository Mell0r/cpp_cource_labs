#include "doctest.h"
#include <string>
#include <ctime>

#include "bit_IO.h"

using std::string;

const string test_file = "../test/test_temp";

TEST_SUITE("bit_IO") {

    TEST_CASE("byte-rw") {
        char c = char(0);
        for (int i = 0; i < 256; i++) {
            CAPTURE(i);

            auto writer = bit_IO::Bit_writer(test_file);
            writer.write_byte(c);
            writer.~Bit_writer();
            auto reader = bit_IO::Bit_reader(test_file);
            CHECK_EQ(reader.read_byte(), c);
            c++;
        }
    }

    TEST_CASE("short-rw") {
        srand(time(0));

        for (int i = 0; i < 50; i++) {
            auto writer = bit_IO::Bit_writer(test_file);
            short cur = rand() % (1 << (2 * bit_IO::BYTE_SIZE));
            CAPTURE(cur);

            writer.write_short(cur);
            writer.~Bit_writer();
            auto reader = bit_IO::Bit_reader(test_file);
            CHECK_EQ(reader.read_short(), cur);
        }
    }

    TEST_CASE("bit-string-rw") {
        srand(time(0));

        for (int i = 0; i < 25; i++) {
            auto writer = bit_IO::Bit_writer(test_file);
            int len = rand() % 5 * 8;
            string cur;
            for (int j = 0; j < len; j++)
                cur += '0' + rand() % 2;
            CAPTURE(cur);

            writer.write_bit_string(cur);
            writer.~Bit_writer();
            auto reader = bit_IO::Bit_reader(test_file);
            string res;
            try {
                while (true)
                    res += '0' + reader.read_bit();
            } catch (const bit_IO::IO_Exception& e) {}

            CHECK_EQ(cur, res);
        }
    }
}