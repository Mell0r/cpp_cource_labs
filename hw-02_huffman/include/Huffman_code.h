#ifndef HW_02_HUFFMAN_HUFFMAN_CODE_H
#define HW_02_HUFFMAN_HUFFMAN_CODE_H

#include <vector>
#include <map>
#include <string>

namespace huffman_code {
    namespace utils {
        const int NONE = -1;

        struct Tree_node {
            int char_quantity;
            int l, r;

            Tree_node() : char_quantity(0), l(-1), r(-1) {}
            explicit Tree_node(int char_quantity) noexcept : char_quantity(char_quantity), l(-1), r(-1) {}
            Tree_node(int char_quantity, int l, int r) noexcept : char_quantity(char_quantity), l(l), r(r) {}
        };
    }

    struct Huffman_code {
        [[nodiscard]] int calc_additional_info_size() const noexcept;
        std::vector<utils::Tree_node> tree;
        std::vector<char> chars;
        std::map<char, std::string> code;

        explicit Huffman_code(const std::map<char, int> &chars) noexcept;
        Huffman_code(const std::vector<char> &chars, const std::vector<std::pair<int, int>> &children) noexcept;

    private:
        void calc_codes(int pos, const std::string& cur_code = "");
    };
}

#endif //HW_02_HUFFMAN_HUFFMAN_CODE_H
