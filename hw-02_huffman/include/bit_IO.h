#ifndef HW_02_HUFFMAN_BIT_IO_H
#define HW_02_HUFFMAN_BIT_IO_H

#include <bitset>
#include <string>
#include <vector>
#include <fstream>


namespace bit_IO {
    const int BYTE_SIZE = 8;
    struct IO_Exception : public std::logic_error {
        explicit IO_Exception(const std::string &reason) : std::logic_error(reason) {}
    };

    class Bit_reader {
    private:
        std::ifstream file;
        char buffer;
        int cur_bit_rem;
        void update_buffer();
    public:
        ~Bit_reader();
        explicit Bit_reader(const std::string& filename);

        void to_begin();

        char read_byte();
        bool read_bit();
        short read_short();
    };

    struct Bit_writer {
    private:
        std::ofstream file;
        char buffer;
        int cur_bit_writen;
    public:
        ~Bit_writer();
        explicit Bit_writer(const std::string& filename);

        void write_bit_string(const std::string& s);
        void write_short(short n);
        void write_byte(char byte);
        void write_bit(bool bit);
        void push_buffer();
    };
}

#endif //HW_02_HUFFMAN_BIT_IO_H
