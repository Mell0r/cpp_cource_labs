#ifndef HW_02_HUFFMAN_HUFFMAN_ARCHIVER_H
#define HW_02_HUFFMAN_HUFFMAN_ARCHIVER_H

#include <string>
#include <stdexcept>

namespace huffman_archiver {
    struct Coding_log {
        int in_size;
        int out_size;
        int additional_size;

        Coding_log() = default;
        Coding_log(int in_size, int out_size, int additional_size) :
            in_size(in_size), out_size(out_size), additional_size(additional_size) {}
    };

    struct Huffman_archiver {
        static Coding_log encode(const std::string &in_file, const std::string &out_file);

        static Coding_log decode(const std::string &in_file, const std::string &out_file);

        static Huffman_archiver& get_instance() {
            static Huffman_archiver archiver;
            return archiver;
        }
    };
}

#endif //HW_02_HUFFMAN_HUFFMAN_ARCHIVER_H
