#include <bmp.h>

void* checked_malloc(size_t size) {
    void* ptr = malloc(size);
    if (ptr == NULL) {
        printf("Cannot allocate memory!\n");
        exit(1);
    }
    return ptr;
}

void checked_fread(void* ptr, size_t type_size, size_t quantity, FILE* file) {
    if (fread(ptr, type_size, quantity, file) != quantity) {
        printf("Cannot read from file!\n");
        exit(2);
    }
}

void checked_fwrite(void* ptr, size_t type_size, size_t quantity, FILE* file) {
    if (fwrite(ptr, type_size, quantity, file) != quantity) {
        printf("Cannot write to file!\n");
        exit(3);
    }
}

FILE* checked_fopen(char* path, char* mode) {
    FILE* file = fopen(path, mode);
    if (file == NULL) {
        printf("Cannot open file!\n");
        fclose(file);
        exit(4);
    }
    return file;
}

void checked_fclose(FILE* file) {
    if (fclose(file) != 0) {
        printf("Cannot close file!\n");
        exit(9);
    }
}

LONG get_padding(BMP_image* image) {
    return (4 - (image->info->width * sizeof(Pixel) % 4)) % 4;
}

BMP_image* bmp_image_from_file(char* file_path) {
    FILE* image_file = checked_fopen(file_path, "rb");

    BMP_image* image = checked_malloc(sizeof(BMP_image));

    image->file_header = checked_malloc(sizeof(Bitmap_file_header));
    checked_fread(image->file_header, sizeof(Bitmap_file_header), 1, image_file);

    image->info = checked_malloc(sizeof(Bitmap_info));
    checked_fread(image->info, sizeof(Bitmap_info), 1, image_file);
    
    image->pixels_inner = checked_malloc(image->info->height * image->info->width * sizeof(Pixel));
    image->pixels = checked_malloc(image->info->height * sizeof(Pixel*));

    size_t padding = get_padding(image);
    BYTE* trash_can = checked_malloc(padding);
    for (int r = image->info->height - 1; r >= 0; r--) {
        image->pixels[r] = image->pixels_inner + r * image->info->width;

        checked_fread(image->pixels[r], sizeof(Pixel), image->info->width, image_file);

        checked_fread(trash_can, 1, padding, image_file);
    }
    free(trash_can);

    checked_fclose(image_file);
    return image;
}

void update_image(BMP_image* image, size_t width, size_t height, Pixel** new_pixels, Pixel* new_inner) {
    free(image->pixels_inner);
    free(image->pixels);

    image->info->width = width;
    image->info->height = height;
    image->info->size_image = (image->info->width * sizeof(Pixel) + get_padding(image)) * height;
    image->pixels = new_pixels;
    image->pixels_inner = new_inner;
    image->file_header->size = sizeof(Bitmap_file_header) + sizeof(Bitmap_info) + image->info->size_image;
}

BMP_image* crop(BMP_image* image, size_t x, size_t y, size_t width, size_t height) {
    Pixel* pixels_cropped_inner = checked_malloc(width * height * sizeof(Pixel));
    Pixel** pixels_cropped = checked_malloc(height * sizeof(Pixel*));

    for (size_t r = 0; r < height; r++) {
        pixels_cropped[r] = pixels_cropped_inner + r * width;

        for (size_t c = 0; c < width; c++)
            pixels_cropped[r][c] = image->pixels[y + r][x + c];
    }
    
    update_image(image, width, height, pixels_cropped, pixels_cropped_inner);
    return image;
}

BMP_image* rotate(BMP_image* image) {
    size_t width  = image->info->width;
    size_t height = image->info->height;

    Pixel* pixels_rotated_inner = checked_malloc(width * height * sizeof(Pixel));
    Pixel** pixels_rotated = checked_malloc(width * sizeof(Pixel*));
    
    for (size_t r = 0; r < width; r++) {
        pixels_rotated[r] = pixels_rotated_inner + r * height;
        for (size_t c = 0; c < height; c++)
            pixels_rotated[r][c] = image->pixels[height - 1 - c][r];
    }

    update_image(image, height, width, pixels_rotated, pixels_rotated_inner);
    return image;
}

void bmp_image_to_file(BMP_image* image, char* path) {
    FILE* image_file = checked_fopen(path, "wb");

    checked_fwrite(image->file_header, sizeof(Bitmap_file_header), 1, image_file);
    checked_fwrite(image->info, sizeof(Bitmap_info), 1, image_file);

    size_t padding = get_padding(image);
    BYTE* line_padding = checked_malloc(padding);
    for (size_t i = 0; i < padding; i++)
        line_padding[i] = 0;

    for (int r = image->info->height - 1; r >= 0; r--) {
        checked_fwrite(image->pixels[r], sizeof(Pixel), image->info->width, image_file);
        checked_fwrite(line_padding, 1, padding, image_file);
    }
    free(line_padding);

    checked_fclose(image_file);
}

void free_image(BMP_image* image) {
    free(image->info);
    free(image->file_header);
    free(image->pixels_inner);
    free(image->pixels);
    free(image);
}