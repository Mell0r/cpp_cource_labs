#include <bmp.h>
#include <stego.h>
#include <string.h>

int main(int argc, char* argv[]) {
    if (argc < 3) {
        printf("Too few arguments!\n");
        return 8;
    }

    BMP_image* image = bmp_image_from_file(argv[2]);
    if (strcmp(argv[1], "crop-rotate") == 0) {
        if (argc != 8) {
            printf("Incorrect number of arguments! Must be 6 for 'crop-rotate'.\n");
            return 8;
        }

        int x = atoi(argv[4]);
        int y = atoi(argv[5]);
        int w = atoi(argv[6]);
        int h = atoi(argv[7]);
        if (x < 0 || w < 1 || x + w > image->info->width) {
            printf("Incorrect width parameters!\n");
            return 5;
        }
        if (y < 0 || h < 1 || y + h > image->info->height) {
            printf("Incorrect height parameters!\n");
            return 6;
        }
        rotate(crop(image, x, y, w, h));
        bmp_image_to_file(image, argv[3]);
    }
    else if (strcmp(argv[1], "insert") == 0) {
        if (argc != 6) {
            printf("Incorrect number of arguments! Must be 4 for 'insert'.\n");
            return 8;
        }
        insert(image, argv[4], argv[5]);
        bmp_image_to_file(image, argv[3]);
    }
    else if (strcmp(argv[1], "extract") == 0) {
        if (argc != 5) {
            printf("Incorrect number of arguments! Must be 3 for 'extract'.\n");
            return 8;
        }
        extract(image, argv[3], argv[4]);
    }
    else 
        return 1;
    
    free_image(image);
    return 0;
}