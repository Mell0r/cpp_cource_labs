#include <stego.h>

typedef struct Key {
    int x, y;
    char color;
} Key;

BYTE get_next_char_code(FILE* message_file, int* char_code) {
    char inp;
    if (fscanf(message_file, "%c", &inp) != 1 || inp == '\n')
        return 0;

    if (inp == ' ')
        *char_code = 26;
    else if (inp == '.')
        *char_code = 27;
    else if (inp == ',')
        *char_code = 28;
    else if (inp < 'A' || inp > 'Z') {
        printf("Message is incorrect!\n");
        exit(10);
    }
    else
        *char_code = inp - 'A';

    return 1;
}

void write_char_code(FILE* message_file, int char_code) {
    if (char_code == 26)
        fprintf(message_file, " ");
    else if (char_code == 27)
        fprintf(message_file, ".");
    else if (char_code == 28)
        fprintf(message_file, ",");
    else if (char_code < 0 || char_code > 28) {
        printf("Char code is incorrect!\n");
        exit(14);
    }
    else
        fprintf(message_file, "%c", 'A' + char_code);
}

BYTE get_next_key(FILE* key_file, Key* key) {
    if (fscanf(key_file, "%i %i %c", &(key->x), &(key->y), &(key->color)) != 3)
        return 0;
    return 1;
}

void hide(BMP_image* image, BYTE info, Key key) {
    if (info == 0) {
        if (key.color == 'R')
            image->pixels[key.y][key.x].red &= BYTE_MAX - 1;
        else if (key.color == 'G')
            image->pixels[key.y][key.x].green &= BYTE_MAX - 1;
        else if (key.color == 'B')
            image->pixels[key.y][key.x].blue &= BYTE_MAX - 1;
    }
    else {
        if (key.color == 'R')
            image->pixels[key.y][key.x].red |= 1;
        else if (key.color == 'G')
            image->pixels[key.y][key.x].green |= 1;
        else if (key.color == 'B')
            image->pixels[key.y][key.x].blue |= 1;
    }
}

int find(BMP_image* image, Key key) {
    //printf("%i %i %i\n", image->pixels[key.y][key.x].red, image->pixels[key.y][key.x].green, image->pixels[key.y][key.x].blue);
    if (key.color == 'R')
        return image->pixels[key.y][key.x].red & 1;
    if (key.color == 'G')
        return image->pixels[key.y][key.x].green & 1;
    if (key.color == 'B')
        return image->pixels[key.y][key.x].blue & 1;
    return -1;
}

BMP_image* insert(BMP_image* image, char* key_path, char* message_path) {
    FILE* message_file = checked_fopen(message_path, "r");
    FILE* key_file = checked_fopen(key_path, "r");
    int char_code;
    Key key;
    while (get_next_char_code(message_file, &char_code)) {
        for (int i = 0; i < 5; i++) {
            if (get_next_key(key_file, &key) == 0) {
                printf("Key is too small!\n");
                exit(11);
            }
            // printf("%i %i %c\n", key.x, key.y, key.color);
            // printf("%i %i %i\n", image->pixels[key.y][key.x].red, image->pixels[key.y][key.x].green, image->pixels[key.y][key.x].blue);
            // printf("%i\n", char_code & 1);
            hide(image, char_code & 1, key);
            // printf("%i %i %i\n============\n", image->pixels[key.y][key.x].red, image->pixels[key.y][key.x].green, image->pixels[key.y][key.x].blue);
            char_code >>= 1;
        }
    }
    checked_fclose(message_file);
    checked_fclose(key_file);
    return image;
}

void extract(BMP_image* image, char* key_path, char* message_path) {
    FILE* message_file = checked_fopen(message_path, "w");
    FILE* key_file = checked_fopen(key_path, "r");
    Key key;
    while (get_next_key(key_file, &key)) {
        int char_code = find(image, key);
        // printf("%i ", char_code);
        for (int i = 1; i < 5; i++) {
            if (get_next_key(key_file, &key) == 0) {
                printf("Number of keys is not divisible by 5!\n");
                exit(12);
            }
            int a = find(image, key);
            // printf("%i ", a);
            char_code += a << i;
        }
        // printf("\n");
        write_char_code(message_file, char_code);
    }

    fprintf(message_file, "\n");
    checked_fclose(message_file);
    checked_fclose(key_file);
}