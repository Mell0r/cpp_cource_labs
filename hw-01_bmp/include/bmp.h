#ifndef _BMPH
#define _BMPH

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define BYTE uint8_t
#define WORD uint16_t
#define DWORD uint32_t
#define LONG int32_t
#define BYTE_MAX 255

#pragma pack(1)
typedef struct Bitmap_file_header {
    WORD type;
    DWORD size;
    WORD reserved1;
    WORD reserved2;
    DWORD off_bits;
} Bitmap_file_header;

#pragma pack(1)
typedef struct Bitmap_info {
    DWORD size; 
    LONG width; 
    LONG height;
    WORD planes;
    WORD bit_count;
    DWORD compression;
    DWORD size_image;
    LONG x_pels;
    LONG y_pels;
    DWORD clr_used;
    DWORD clr_important;
} Bitmap_info;

#pragma pack(1)
typedef struct Pixel {
    BYTE blue;
    BYTE green;
    BYTE red;
} Pixel;

#pragma pack(1)
typedef struct BMP_image {
    Bitmap_file_header* file_header;
    Bitmap_info* info;
    Pixel** pixels;
    Pixel* pixels_inner;
} BMP_image;

BMP_image* bmp_image_from_file(char* file_path);
BMP_image* crop(BMP_image* image, size_t x, size_t y, size_t width, size_t height);
BMP_image* rotate(BMP_image* image);
void bmp_image_to_file(BMP_image* image, char* path);
void free_image(BMP_image* image);
FILE* checked_fopen(char* path, char* mode);
void checked_fclose(FILE* file);

#endif