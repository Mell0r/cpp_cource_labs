#ifndef _STEGOH
#define _STEGOH

#include <bmp.h>

BMP_image* insert(BMP_image* image, char* key_path, char* message_path);
void extract(BMP_image* image, char* key_path, char* message_path);

#endif